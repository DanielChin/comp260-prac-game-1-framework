﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class PlayerMove : MonoBehaviour {

	private BeeSpawner beeSpawner;

	// Use this for initialization
	void Start () {
		beeSpawner = FindObjectOfType<BeeSpawner> ();
	}


	public string inputH;
	public string inputV;
	public float maxSpeed = 5.0f;
	public float acceleration = 9.0f; // in metres/second/second
	public float brake = 5.0f; // in metres/second/second
	public float turnSpeed = 30.0f; // in degrees/second
	private float speed = 0.0f;    // in metres/second
	public float destroyRadius = 1.5f;
	void Update() {
		if (Input.GetButton ("Fire1")) {
			// destroy nearby bees
			beeSpawner.DestroyBees(
				transform.position, destroyRadius);
		}

		// the horizontal axis controls the turn
		float turn = Input.GetAxis(inputH);

		// turn the car
		if (speed > 0) {

			transform.Rotate (0, 0, turn * -1 * turnSpeed * Time.deltaTime * speed/8);
		
		} else {

			transform.Rotate (0, 0, turn * -1 * turnSpeed * Time.deltaTime * speed/8);
		
		}

		// the vertical axis controls acceleration fwd/back
		float forwards = Input.GetAxis(inputV);
		if (forwards > 0) {
			// accelerate forwards
			speed = speed + acceleration * Time.deltaTime;
		}
		else if (forwards < 0) {
			// accelerate backwards
			speed = speed - acceleration * Time.deltaTime;
		}
		else {
			// braking
			if (speed > 0) {
				speed = speed - brake * Time.deltaTime;
				if (speed < 0) {
					speed = 0;
				}
			} else {
				speed = speed + brake * Time.deltaTime;
				if (speed > 0) {
					speed = 0;
				}
			}
		}


		speed = Mathf.Clamp(speed, -maxSpeed, maxSpeed);

		// compute a vector in the up direction of length speed
		Vector2 velocity = Vector2.up * speed;

		// move the object
		transform.Translate(velocity * Time.deltaTime, Space.Self);
	}
}
