﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeeMove : MonoBehaviour {

	public float minSpeed, maxSpeed;
	public float minTurnSpeed, maxTurnSpeed;

	private float speed = 4.0f;        // metres per second
	private float turnSpeed = 180.0f;  // degrees per second
	public Transform target1;
	public Transform target2;
	public Vector2 heading = Vector3.right;



	// Use this for initialization

	void Start () {
		// find a player object to be the target by type
		// Note: this is not standard Unity syntax
		PlayerMove player = FindObjectOfType<PlayerMove>();
		target1 = player.transform;
		target2 = player.transform;

		heading = Vector2.right;
		float angle = Random.value * 360;
		heading = heading.Rotate(angle);

		// set speed and turnSpeed randomly 
		speed = Mathf.Lerp(minSpeed, maxSpeed, Random.value);
		turnSpeed = Mathf.Lerp(minTurnSpeed, maxTurnSpeed, Random.value);
		
	}



	void Update() {
		// get the vector from the bee to the target 
		Vector2 direction1 = target1.position - transform.position;
		Vector2 direction2 = target2.position - transform.position;

		// calculate how much to turn per frame
		float angle = turnSpeed * Time.deltaTime;

		if (direction1.magnitude < direction2.magnitude) {

			// turn left or right
			if (direction1.IsOnLeft (heading)) {
				// target on left, rotate anticlockwise
				heading = heading.Rotate (angle);
			} else {
				// target on right, rotate clockwise
				heading = heading.Rotate (-angle);
			}
		} 
		else {
			// turn left or right
			if (direction2.IsOnLeft (heading)) {
				// target on left, rotate anticlockwise
				heading = heading.Rotate (angle);
			} else {
				// target on right, rotate clockwise
				heading = heading.Rotate (-angle);
			}
		}

		transform.Translate(heading * speed * Time.deltaTime);
	}

	void OnDrawGizmos() {
		// draw heading vector in red
		Gizmos.color = Color.red;
		Gizmos.DrawRay(transform.position, heading);

		// draw target vector in yellow
		Gizmos.color = Color.blue;
		Vector2 direction = target1.position - transform.position;
		Gizmos.DrawRay(transform.position, direction);

		Gizmos.color = Color.green;
		Vector2 direction2 = target2.position - transform.position;
		Gizmos.DrawRay(transform.position, direction2);
	}
	public ParticleSystem explosionPrefab;
	void OnDestroy() {
		// create an explosion at the bee's current position
		ParticleSystem explosion = Instantiate(explosionPrefab);
		explosion.transform.position = transform.position;
		// destroy the particle system when it is over
		Destroy(explosion.gameObject, explosion.duration);
	}


}

