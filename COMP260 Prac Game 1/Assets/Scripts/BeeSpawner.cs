﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeeSpawner : MonoBehaviour {

	public int nbees = 50; 
	public BeeMove beePrefab;
	public float xMin, yMin;
	public float width, height;
	public float respawnSeconds;
	private float nextBee = 0.0f;
	public float minBeePeriod;
	public float maxBeePeriod;

	// Use this for initialization
	void Start () {
		for (int i = 0; i < nbees; i++){
			BeeMove bee = Instantiate(beePrefab);
			// attach to this object in the hierarchy
			bee.transform.parent = transform;            
			// give the bee a name and number
			bee.gameObject.name = "Bee " + i;

			float x = xMin + Random.value * width;
			float y = yMin + Random.value * height;
			bee.transform.position = new Vector2(x,y);
		}

	}
	
	// Update is called once per frame
	void Update () {
		if (Time.time > nextBee){
			BeeMove bee = Instantiate(beePrefab);
			// attach to this object in the hierarchy
			bee.transform.parent = transform;            
			// give the bee a name and number
			bee.gameObject.name = "Bee " + "Extra";

			float x = xMin + Random.value * width;
			float y = yMin + Random.value * height;
			bee.transform.position = new Vector2(x,y);

			nextBee = Time.time + minBeePeriod + (Random.value * (maxBeePeriod - minBeePeriod));
		}
	}

	public void DestroyBees(Vector2 centre, float radius) {
		// destroy all bees within ‘radius’ of ‘centre’
		for (int i = 0; i < transform.childCount; i++) {
			Transform child = transform.GetChild(i);
			// BUG! the line below doesn’t work
			Vector2 v = (Vector2)child.position - centre;
			if (v.magnitude <= radius) {
				Destroy(child.gameObject);
			}
		}
	}
}
